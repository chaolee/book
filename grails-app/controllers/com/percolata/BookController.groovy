package com.percolata

class BookController {

    def index() {
        Info info = new Info(
                name: "nname",
                age: 24
        )
        Book book = new Book(
                title: "ttile",
                content: "cconten",
        )
        book.info = info
        render book
    }
}
