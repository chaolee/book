package com.percolata

import grails.converters.JSON

class BaseDomain {

    static constraints = {
    }

    String gukey

    @Override
    String toString() {
        return this as JSON
    }
}
