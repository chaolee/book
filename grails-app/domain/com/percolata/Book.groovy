package com.percolata

class Book extends BaseDomain{

    static constraints = {
        title(nullable: true)
        content(nullable: true)
    }

    String title
    String content

    Info info

}
